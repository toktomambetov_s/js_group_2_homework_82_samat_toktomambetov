const express = require('express');

const ObjectId = require('mongodb').ObjectId;

const Album = require('../models/Album');

const createRouter = db => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Album.find()
            .then(albums => res.send(albums))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', (req, res) => {
        const album = new Album(req.body);

        album.save()
            .then(album => res.send(album))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        db.collection('albums')
            .findOne({_id: new ObjectId(req.params.id)})
            .then(result => {
                if (result) res.send(result);
                else res.status(404).send({error: 'Not found'});
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter();