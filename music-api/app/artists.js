const express = require('express');

const Artist = require('../models/Artist');

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Artist.find()
            .then(artists => res.send(artists))
            .catch(() => res.sendStatus(500));
    });

    router.post('/', (req, res) => {
        const artist = new Artist(req.body);

        artist.save()
            .then(artist => res.send(artist))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter();