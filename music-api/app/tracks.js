const express = require('express');

const Track = require('../models/Track');

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {
        Track.find()
            .then(tracks => res.send(tracks))
            .catch(() => res.sendStatus(500));
    });

};

module.exports = createRouter;